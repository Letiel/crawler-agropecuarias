<!DOCTYPE html>
<html>
	<head>
		<title>Letiel.test</title>
		<meta charset='utf-8' />
		<!-- <link rel="stylesheet" type="text/css" href="/css/bootstrap.css"> -->
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<!-- // <script type="text/javascript" src='/js/jquery-2.1.4.min.js'></script> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<!-- // <script type="text/javascript" src='/js/bootstrap.min.js'></script> -->
		<script type="text/javascript" src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
		<style type="text/css">
			.br{
				display: block;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
		<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
	</head>
	<body>
		<div class="row panel panel-default col-md-10 col-md-offset-1">
			<div class="panel-body">
				<form action='#' method='post'>
					<div class="form-group">
						<input required='required' class="form-control" id='url_cidade' />
					</div>

					<div class="form-group">
						<?php include 'class/bd.php';
								if(!$banco)
									echo "Não conectou no banco de dados!"; ?>
						<select id="cidade" class='form-control'>
							<option value=''>Selecione</option>
							<?php
								$query = mysqli_query($banco, "SELECT * FROM lc_terms JOIN lc_term_taxonomy ON (lc_term_taxonomy.term_id = lc_terms.term_id) WHERE lc_term_taxonomy.parent = 0");
								while($categoria = mysqli_fetch_array($query)){
									echo utf8_encode("<option value='$categoria[term_id]'>$categoria[name]</option>");
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<button id='submit' class="btn btn-primary" type='submit'>Enviar</button>
					</div>
				</form>
			</div>
		</div>
		<div class="row panel panel-default col-md-10 col-md-offset-1">
			<div class="panel-body">
				<div class='alert alert-info' id='aguarde' style='display: none;'>
					<b>Aguarde...</b>
					<img src='/crawler_agro/img/loading.gif' style='width: 170px;' />
				</div>
			</div>
			<div id="resultado"></div>
		</div>
		<audio id="audio">
		    <source src="alarme.mp3" type="audio/mpeg">
		    <!-- <source src="i_am_the_doctor.mp3" type="audio/mpeg"> -->
		    Seu navegador não possui suporte ao elemento audio
		</audio>
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

		<script type="text/javascript">

			function loop_total(){
				$.post("/crawler_agro/loop_total.php", {

				}, function(result){
					$("#total_cadastros").html(result);
					document.title = result;
				});
			}

			// setInterval(loop_total, 3000);

			$(document).ready(function(){

				$('form').submit(function(){
					console.log($("#url_cidade").val());
					console.log($("#cidade").val());
					$("#aguarde").show();
					$.post("/crawler_agro/crawler_geral.php", 
					{
						url_cidade: $("#url_cidade").val(),
						cidade: $("#cidade").val(),
					},
					function(result){
							$('#cidade').prop('selectedIndex',0);
							$("#url_cidade").val("");
				        	$("#resultado").html(result);
				        	$("#aguarde").hide();
				        	play();
				    });
					return false;
				});

				$("#cidade").select2();
			});

			audio = document.getElementById('audio');
						 
		    function play(){
		        audio.play();
		    }
		</script>
	</body>
</html>